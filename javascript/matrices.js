import { matrix, add, subtract, multiply, divide } from "mathjs";

const mA = matrix([
	[1, 2],
	[3, 4],
]);
const mB = matrix([
	[5, 6],
	[7, 8],
]);

const matrixResult1 = add(mA, mB);
const matrixSubst = subtract(mA, mB);
const matrixMultiply = multiply(mA, mB);
const matrixDivision = divide(mA, mB);

console.log("addition", matrixResult1.toArray());
console.log("subtraction", matrixSubst.toArray());
console.log("multiplication", matrixMultiply.toArray());
console.log("division", matrixDivision.toArray());

