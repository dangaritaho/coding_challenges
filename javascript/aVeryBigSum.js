function aVeryBigSum(array){
    let result = 0
    for (let i = 0; i < array.length; i++) {
        result += array[i]
    }
    return result
}

console.log(aVeryBigSum([1000000001, 1000000002, 1000000003, 1000000004, 1000000005]))


