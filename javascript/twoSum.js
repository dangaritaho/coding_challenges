function twoSum(array, target) {
    // create a result empty array to store the 2 numbers that when combined
    // will be equal to the target
    // loop through the array
    //     is the sum of the first element with the following element is equal to the target
    //         push the indexes into the result array
    // return the indexes of the two numbers

    let result = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i] + array[i+1] === target) {
            result.push(i, i+1)
        }
    }
    return result
}

console.log(twoSum([2,7,11,15], 9))
console.log(twoSum([3,2,4], 6));
console.log(twoSum([3, 3], 6));
