function diagonalDifference(array) {
    let primSum = 0
    let secSum = 0

    for (let i = 0; i < array.length; i++) {
        primSum = primSum + array[i][i]
        console.log(primSum)
    }
    for (let j = 0; j < array.length; j++) {
        secSum = secSum + array[j][array.length - 1 - j]
        console.log(secSum)
    }
    return Math.abs(primSum - secSum)
}

console.log(diagonalDifference([[11,2,4], [4,5,6], [10,8,-12]]))
