function compareTriplets(a, b) {
    let alicePoints = 0
    let bobPoints = 0
    if (a.length === b.length) {
        for (let i = 0; i < b.length; i++) {
            if (a[i] > b[i]) {
                alicePoints += 1
            }
            if (a[i] < b[i]) {
                bobPoints += 1
            }
            continue
        }
    }
    return [alicePoints, bobPoints]
}

console.log(compareTriplets([1, 2, 3], [3, 2, 1]));
