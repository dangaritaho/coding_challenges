names = ['Alice', 'Bob', 'Carol', 'Dan', 'Eve']
ages = [22, 32, 18, 57, 41]
current_year = 2017

for person in range(6-1):
    age = ages[person]
    name = names[person]
    year_of_birth =  (current_year) - (age)
    print(f"{name} was born in {year_of_birth}")
    print(name, "was born in", year_of_birth)
