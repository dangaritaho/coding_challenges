def test_fibonacci():
    test_cases = [
        (0, 0),
        (1, 1),
        (2, 1),
        (3, 2),
        (4, 3),
        (5, 5),
        (6, 8),
        # Add more test cases if needed
    ]

    for n, expected_output in test_cases:
        result = fibonacci(n)
        assert result == expected_output, f"fibonacci({n}) returned {result}, expected {expected_output}"

    print("All test cases passed!")


def fibonacci(n, memo={}):
    if n in memo:
        return memo[n]

    if n == 0:
        memo[n] = 0
    elif n <= 2:
        memo[n] = 1
    else:
        memo[n] = fibonacci(n - 1, memo) + fibonacci(n - 2, memo)

    return memo[n]


test_fibonacci()
