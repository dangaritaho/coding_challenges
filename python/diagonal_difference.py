def diagonal_difference(arr):
  prim_sum = 0
  sec_sum = 0

  for i in range(0, len(arr)):
    prim_sum = prim_sum + arr[i][i]
    print(prim_sum)

  for j in range(0, len(arr)):
    sec_sum = sec_sum + arr[j][len(arr)-1-j]
    print(sec_sum)
  return abs(prim_sum - sec_sum)

print(diagonal_difference([[11,2,4], [4,5,6], [10,8,-12]]))
