def detect_arbitrage_opportunities(price_data):
    opportunities = []

    for exchange1 in price_data:
        for exchange2 in price_data:
            if exchange1 != exchange2:
                for buying_price in price_data[exchange1]['buy']:
                    for selling_price in price_data[exchange1]['sell']:
                        if buying_price < selling_price:
                            profit = selling_price - buying_price
                            opportunity = {
                                'exchange1': exchange1,
                                'exchange2': exchange2,
                                'buying_price': buying_price,
                                'selling_price': selling_price,
                                'profit': profit
                            }
                            opportunities.append(opportunity)
    return opportunities


price_data = {
    'Exchange1': {
        'buy': [10, 12, 15],
        'sell': [13, 14, 16]
    },
    'Exchange2': {
        'buy': [11, 14, 13],
        'sell': [15, 16, 17]
    }
}

arbitrage_opportunities = detect_arbitrage_opportunities(price_data)
if arbitrage_opportunities:
    for opportunity in arbitrage_opportunities:
        print("Arbitrage Opportunity:")
        print(f"Buy from {opportunity['exchange1']} at {opportunity['buying_price']}")
        print(f"Sell on {opportunity['exchange2']} at {opportunity['selling_price']}")
        print(f"Profit: {opportunity['profit']}")
        print()
else:
    print("No arbitrage opportunities found.")
