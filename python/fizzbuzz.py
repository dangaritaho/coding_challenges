def fizzBuzz(n):
  for i in range(1, n + 1):
    if i % 3 == 0 and i % 5 == 0: print("FizzBuzz")
    elif i % 3 == 0: print("Fizz")
    elif i % 5 == 0: print("Buzz")
    else: print(i)

print(fizzBuzz(15))






from math import comb

def count_reverse_ways(s, k):
    n = len(s)
    count = 0

    for i in range(n//2 + 1):
        flips = s[i] == '0'  # Count flips needed for this character to be in the correct position
        if flips > k:
            continue

        # Calculate the number of ways to choose the remaining flips for the other character in the pair
        remaining_flips = k - flips
        count += 2 ** max(0, remaining_flips)

        # If the characters are the same, we can only consider one of them, as they are already in the correct order
        if s[i] == s[n - 1 - i]:
            count -= 1

    return count

# Example usage:
s = "ababa"
k = 2
result = count_reverse_ways(s, k)
print(result)  # Output: 2

def max_picked_products(numproducts):
    n = len(numproducts)
    dp = [1] * n  # Initialize DP array with 1, as we can always pick one product from any pile

    for i in range(1, n):
        for j in range(i):
            # Check if the condition is satisfied (pick strictly fewer products from the ith pile)
            if numproducts[i] > numproducts[j]:
                dp[i] = max(dp[i], dp[j] + 1)

    return max(dp)

# Example usage:
numproducts = [4,2,5,6,7]
result = max_picked_products(numproducts)
print(result)  # Output: 20


def max_picked_products(numproducts):
    n = len(numproducts)
    max_products = 0  # Variable to store the maximum number of products picked

    for i in range(n -1):
        # Check if the number of products in the (i+1)th pile is greater than the ith pile
        if numproducts[i + 1] < numproducts[i]:
            # Pick the maximum number of products from the (i+1)th pile
            max_products += numproducts[i + 1]

    return max_products

# Example usage:
numproducts = [4, 2, 5, 6, 7]
result = max_picked_products(numproducts)
print(result)  # Output: 20
