def compare_triplets(a, b):
  alice_points = 0
  bob_points = 0
  if len(a) == len(b):
    for i in range(0, len(b)):
      if a[i] > b[i]: alice_points += 1
      if a[i] < b[i]: bob_points += 1
      continue
  return [alice_points, bob_points]

print(compare_triplets([1, 2, 3], [3, 2, 1]))
