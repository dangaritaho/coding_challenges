def pair_up(items):
    chunks = []
    in_progress = []
    if len(items) %2 != 0:
        items.pop()
    for i in range(0, len(items), 2):
        in_progress = [items[i], items[i + 1]]
        chunks.append(in_progress)
    return chunks



input = [1, 2, 3, 4]

result = pair_up(input)
print(result)
