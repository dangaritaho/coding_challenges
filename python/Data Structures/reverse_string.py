def reverse_string(string):
    return string[::-1]

print(reverse_string("I want to reverse the string"))

class Stack():
    def __init__(self) -> None:
        self.items = []

    def push(self, items):
        self.items.append(items)

    def pop(self):
        return self.items.pop()

    def get_stack(self):
        return self.items

    def is_empty(self):
        if self.items == []: return True
        return False

    def peek(self):
        if not self.is_empty(): return self.items[-1]
        return None

def reverse_str(string):
    s = Stack()
    reversed_str = ""

    for ele in string:
        s.push(ele)
        # print(s.get_stack())

    while not s.is_empty():
        reversed_str += s.pop()

    return reversed_str


print(reverse_str("Hello"))
