class Node:
    def __init__(self, data) -> None:
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self) -> None:
        self.head = None


    def print_list(self):
        curr_node = self.head # initialize the curr_node to the head of the linked list
        while curr_node: # keep checking the next node until is NONE
            print(curr_node.data) # print the current data of the node
            curr_node = curr_node.next # move to the next node until is None.


    def append(self, data):
        new_node = Node(data)
        if self.head is None: #if the linked list is empty
            self.head = new_node # set the head pointer to the new_node
            return None
        # if the linked list is not empty:
        last_node = self.head # define last_node which was initially equal to head
        # if last_node points to None, the loop will terminates.
        while last_node.next: # while last_node is not Null
            last_node = last_node.next # keep moving from node to node until last_node.next points to None
        last_node.next = new_node # inputs the new_node into the linked list by setting the next of the last_node to new_node


    def prepend(self, data):
        new_node = Node(data)

        new_node.next = self.head
        self.head = new_node


    def insert_after_node(self, prev_node, data):
        if not prev_node:
            print('Previous node does not exist')
            return

        new_node = Node(data)

        new_node.next = prev_node.next
        prev_node.next = new_node


llist = LinkedList()
llist.append('A')
llist.append('B')
llist.append('C')
llist.prepend('D')
llist.append('E')
llist.prepend('G')
llist.append('H')
llist.insert_after_node(llist.head.next, 'f')

llist.print_list()
