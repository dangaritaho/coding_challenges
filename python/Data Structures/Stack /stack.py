"""
    Stack Data structure (LIFO)
"""
# line 5 - 7 : the stack is created.
class Stack():
    def __init__(self):
        # self.items is created when we create a stack object.
        self.items = []

    # line 11-12: created a member of the class, push and.
    # it will take item as an argument.
    def push(self, item):
        self.items.append(item)

    # Pop method returns the last element inserted on the list
    def pop(self):
        return self.items.pop()

    # to check if the stack is empty
    def is_empty(self):
        if self.items == []: return True
        else: return False

    # peek check if the stack is not empty, if is not,
    # it will return the last element of the list
    def peek(self):
        if self.is_empty() != []: return self.items[-1]

    # to return the elements of the stack
    def get_stack(self):
        return self.items




myStack = Stack()
print(myStack.is_empty())
myStack.push("A")
myStack.push("B")
print(myStack.get_stack())
myStack.push("C")
myStack.push("D")
myStack.push("E")
myStack.push("F")
myStack.push("G")
myStack.push("H")
myStack.push("I")
myStack.push("J")
print(myStack.get_stack())
myStack.pop()
print(myStack.get_stack())
print(myStack.peek())
