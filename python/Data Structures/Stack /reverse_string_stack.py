from stack import Stack

def reverse_string(input_string):
    stack = Stack() # declare a new stack object
    reversed_string = "" # initialize a new string to store the reversed string
    for i in range(len(input_string)): #iterate over input string
        char = input_string[i] # declare the current character
        stack.push(char) # push the current character into the stack
    while not stack.is_empty():
        reversed_string += stack.pop() # pop all the elements from the stack until empty
        print(reversed_string)
    print(reversed_string)
    return reversed_string # return the reversed string


print(reverse_string( "Reversing string"))





def reverse_string(stack, input_string):

    reversed_string = "" # initialize a new string to store the reversed string
    for ele in input_string: #iterate over input string
        stack.push(ele) # push the current character into the stack
    while not stack.is_empty():
        reversed_string += stack.pop() # pop all the elements from the stack until empty
        print(reversed_string)
    print(reversed_string)
    return reversed_string # return the reversed string


print(reverse_string(Stack(), "Reversing string"))
