from stack import Stack

def is_match(p1, p2):
    if p1 == "(" and p2 == ")" : return True
    elif p1 == "{" and p2 == "}": return True
    elif p1 == "[" and p2 == "]": return True
    else: return False

def is_paren_balanced(paren_string):
    s = Stack()
    is_balanced = True
    index = 0

    while index < len(paren_string) and is_balanced:
        paren = paren_string[index]
        print(paren)
        if paren in "([{":
            print(paren)
            s.push(paren)
            print(s.get_stack())
        else:
            if s.is_empty():
                is_balanced = False
                break

            else:
                top = s.pop()
                print(top)
                if not is_match(top, paren):
                    is_balanced = False
                    break
        index += 1
    if s.is_empty() and is_balanced: return f"{True}, parenthesis is balanced"
    else: return f"{False}, parenthesis is NOT balanced"


print(is_paren_balanced("(({[]}))"))
print(is_paren_balanced("[][]]]"))
print(is_paren_balanced("[][][][]"))
