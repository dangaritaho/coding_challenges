from stack import Stack

def decimal_to_binary(num):
    # Edge case: if the int is 0, return 0
    # declare a new stack
    # create a new variable that holds the bin_number as a string
    # use a while loop
    # create a variable named remainder and set it equal to the remainder of the int
    # push the remainder into the stack
    # get the division of the num by 2 without remainder
    # loop while the stack is not empty
    # pop the top element from the stack and append it as a string into the bin_num var


    if num == 0: return 0
    s = Stack()
    bin_num = ""

    while num>0:
        remainder = num % 2
        print(remainder)
        s.push(remainder)
        num = num//2
        print(num)

    while not s.is_empty():
        bin_num += str(s.pop())
    return bin_num

print(decimal_to_binary(242))
print(decimal_to_binary(56))
print(decimal_to_binary(2))
print(decimal_to_binary(32))
print(decimal_to_binary(10))

print(int(decimal_to_binary(56),2)==56)
