def fibonacci_generator(n):
    output = []
    if n == 1: output.append(0)
    elif n== 2: output.extend([0, 1])
    else: output.extend([0, 1])
    for i in range(2, n):
        output.append(output[- 2 ] + output[ - 1 ])
    return output


print(fibonacci_generator(50))
