def missingWords(s,t):
    array = []
    splitted_s = s.split()
    splitted_t = t.split()
    i = 0
    j = 0
    for j in range(len(splitted_s)):
        if splitted_s[j] == splitted_t[i]:
            i+=1
            if i >= len(splitted_t):
                break
        else: array.append(splitted_s[j])
    for k in range(j+1, len(splitted_s)): array.append(splitted_s[k])
    return array

def missingWords(s,t):
    missing_words = []
    splitted_s = s.split(' ')
    splitted_t = t.split(' ')
    i = 0
    j = 0
    while i < len(splitted_s):
        if j >= len(splitted_t) or  splitted_s[i] != splitted_t[j]:
            missing_words.append(splitted_s[i])
        else:
            j+=1
        i += 1
    return missing_words



print(missingWords('I like cheese', 'like'))
print(missingWords('I am using JobOverflow to improve programming', 'am JobOverflow'))


print(missingWords('I like cheese', 'like'))
print(missingWords('I am using JobOverflow to improve programming', 'am JobOverflow'))


print(missingWords('I like cheese', 'like'))
print(missingWords('I am using JobOverflow to improve programming ', 'am JobOverflow'))
