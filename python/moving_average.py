def moving_average(closing_prices, period):
    # check if the length of the closing prices is less than or equal to the period
        # if it is, return an error
    # create a moving avr variable to store the result
    # iterate over the closing prices starting from the index period - 1
        # calculate the sum of closing prices for the specified period
        # calculate the moving average
        # append the moving average to the moving_avr list
    # return the moving average list
    if len(closing_prices) <= period:
        return "Insufficient data to calculate moving average."
    moving_avr = []
    for i in range(period -1, len(closing_prices)):
        sum_closing_price = sum(closing_prices[i - period + 1:i + 1])
        moving_average = sum_closing_price / period
        moving_avr.append(moving_average)
    return moving_avr


print(moving_average([10, 12, 15, 14, 13, 12, 11, 10, 9, 10], 3))
