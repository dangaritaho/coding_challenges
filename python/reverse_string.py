def reverse_string(string):
    # solution 1
    # return string[::-1]

    # solution 2
    # reversed_string = ""
    # for char in string:
    #     reversed_string = char + reversed_string
    # return reversed_string

    # solution 3
    reversed_string = "".join(reversed(string))
    return reversed_string


print(reverse_string("Hello World!"))
