def is_palindrome(string):
    # create a var to store the letters of the str
    # iterate over the str and check if the string contains letters, add the letters to the created var and join the str
    # reverse the string and
    # return the comparison it with the original string

    alphabetic_str = ""
    for char in string.lower():
        if char.isalpha():
            alphabetic_str += "".join(char)
    reversed_str = "".join(reversed(alphabetic_str))
    return alphabetic_str == reversed_str


print(is_palindrome("Madam in Eden, I'm Adam."))
print(is_palindrome("Hello World."))
