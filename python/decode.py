def decode(message_file):
    with open(message_file, 'r') as file:
        lines = file.readlines()

    pairs = []
    for line in lines:
        if line.strip():  # Skip blank lines
            num, word = line.strip().split()
            pairs.append((int(num), word))

    pairs.sort(key=lambda x: x[0])

    filtered_words = [word for num, word in pairs if num in {1, 3, 6}]

    decoded_message = " ".join(filtered_words)

    return decoded_message.capitalize()




# Example usage
message_file = "message_file.txt"
decoded_result = decode(message_file)
print(decoded_result)
