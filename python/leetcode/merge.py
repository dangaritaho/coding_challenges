def merge(nums1, m, nums2, n):
        """
        Do not return anything, modify nums1 in-place instead.
        """
        # slice the array beginning on the first element to the target element
        # merge with the num2 elements
        # sort the array
        nums1[m:] = nums2[:n]; nums1.sort()
        return nums1


nums1 = merge([1,2,3,0,0,0], 3, [2,5,6], 3)
print(nums1)
