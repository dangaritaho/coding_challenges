def majority_element_II(nums):

    if not nums:
        return []

    count1 = 0
    count2 = 0
    candidate1 = None
    candidate2 = None
    for n in nums:
        if n == candidate1:
            print(candidate1)
            count1 += 1
        elif n == candidate2 :
            count2 += 1
        elif count1 == 0:
            candidate1 = n
            count1 += 1
        elif count2 == 0:
            candidate2 = n
            count2 += 1
        else:
            count1 -= 1
            count2 -= 1


    result = []
    for c in [candidate1, candidate2]:
        if nums.count(c) > len(nums)//3:
            result.append(c)

    return result



print(majority_element_II([3,2,3]))
print(majority_element_II([1]))
print(majority_element_II([1,2])) # return [1, 2]
print(majority_element_II([0]))
print(majority_element_II([2,2]))
print(majority_element_II([1,2,3])) # output []
