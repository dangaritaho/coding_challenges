import math

def get_prime_numbers(nums):
    result = []

    for num in nums:
        if num < 2:
            continue
        is_prime = True
        for i in range(2, int(math.sqrt(num))+1):
            if num % i == 0:
                is_prime = False
                break
        if is_prime:
            result.append(num)

    return result

print(get_prime_numbers([1,2,3])) # [2,3]

print(get_prime_numbers([1])) # []

print(get_prime_numbers([])) # []

print(get_prime_numbers([1,2,3,4,5,6,7,8,9,10,111111111111,9999999999])) # []
#2,3,5,7
