"""
Given the roots of two binary trees root and subRoot,
return true if there is a subtree of root with the same structure
and node values of subRoot and false otherwise.

A subtree of a binary tree tree is a tree that consists of a node
in tree and all of this node's descendants. The tree tree could also be
considered as a subtree of itself."""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def isSubtree(self, root, subRoot) -> bool:
        def dfs(node):
            if node is None: #if node is null, return false
                return False
            elif isIdentical(node, subRoot): # if trees are identical, return True
                return True
            else:
                if dfs(node.left) or dfs(node.right):
                    return True
                else:
                    return False

        def isIdentical(node1, node2):
            if node1 is None and node2 is None:
                return True

            if node1 is None or node2 is None:
                return False

            return node1.val == node2.val and isIdentical(node1.left, node2.left) and isIdentical(node1.right, node2.right)

        return dfs(root)

root = [3,4,5,1,2]
subRoot = [4,1,2]


# Create the binary tree directly from the list representation
root = TreeNode(root[0], TreeNode(root[1], TreeNode(root[3]), TreeNode(root[4])), TreeNode(root[2]))

# Create the subRoot binary tree directly from the list representation
subRoot = TreeNode(subRoot[0], TreeNode(subRoot[1]), TreeNode(subRoot[2]))

# Instantiate the Solution class
solution = Solution()

# Call the isSubtree method to check if subRoot is a subtree of root
print(solution.isSubtree(root, subRoot))  # Output: True
