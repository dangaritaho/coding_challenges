def climbing_stairs(num):
    if num == 0 or num == 1: return 1
    prev_step = 1
    curr_step = 1
    for i in range(2, num +1):
        temp_step = curr_step
        curr_step = prev_step + curr_step
        prev_step = temp_step # Update 'prev' for the next iteration to be the previous 'curr'
    return curr_step

print(climbing_stairs(2)) #output 2
