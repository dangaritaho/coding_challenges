def count_triplets(nums, k):
    #create a count variable to store the triplets count\
    #iterate through the array of int
    # check if there are 3 int that after adding them up is equal to k
    #If it is, return the count var

    count = 0
    for i in range(len(nums)):
        left = i + 1
        right = len(nums) - 1
        while left < right:
            triplet_sum = nums[i] + nums[left] + nums[right]

            if triplet_sum > k:
                right -= 1
            elif triplet_sum<k:
                left += 1
            else:
                count +=1
                break
    return count



print(count_triplets([1,1,1,2,2], 3))

print(count_triplets([1,2,3,4,5], 6))

print(count_triplets([1, 2, 3, 4, 5], 8))


# solution 2

def count_triplets(nums, k):
    count = 0
    for i in range(len(nums)-2):
        first_num = nums[i]
        for j in range(i+1, len(nums)-1):
            second_num = nums[j]
            for l in range(j+1, len(nums)):  # Renamed 'k' to 'l' to avoid name conflicts
                third_num = nums[l]
                if first_num + second_num + third_num == k:
                    count += 1
    print(count)
    return count


print(count_triplets([1,1,1,2,2], 3))

print(count_triplets([1,2,3,4,5], 6))

print(count_triplets([1, 2, 3, 4, 5], 8))
