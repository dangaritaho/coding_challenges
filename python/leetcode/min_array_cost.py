def get_min_cost(arr):
    cost = 0
    max_cost = 0
    #  max difference between neighbors
    for i in range(len(arr)-1):
        if abs(arr[i+1] - arr[i]) > max_cost:
            max_cost =  abs(arr[i+1] - arr[i])
    # new number between maximum difference
    new_num = (arr[i] + arr[i+1])//2
    cost += ((arr[i] - new_num)**2) + ((arr[i+1] - new_num)**2)
    for j in range(len(arr)-1):
        if j == i:
            continue
        cost += (arr[j] - arr[j+1])**2
    print(cost)
    return cost
print(get_min_cost([1,3,5,2,10]))


def min_cost(arr):
    max_cost = 0
    index = -1
    for i in range(len(arr)-1):
        if abs(arr[i]-arr[i+1]) >= max_cost:
            index = i
    median = (arr[index] + arr[index+1])//2
    initial_cost = 0
    for i in range(len(arr)-1):
        initial_cost+= (arr[i] - arr[i+1])**2

    initial_cost += ((arr[index] - median)**2) + ((median - arr[index+1])**2) -((arr[index] - arr[index+1])**2)
    print(initial_cost)
    return initial_cost

print(min_cost([1,3,5,2,10]))
