def reverse_a_string(arr_str): #solution 1
    arr_str.reverse()
    return arr_str

print(reverse_a_string(['h','e','l','l','o']))


def reverse_a_string(arr_str): #solution 2
    arr_str[:] = arr_str[::-1]
    return arr_str

print(reverse_a_string(['h','e','l','l','o']))


def reverse_a_string(arr_str): #solution 3
    j = len(arr_str)-1
    i = 0
    while i < j:
        arr_str[i], arr_str[j] = arr_str[j], arr_str[i]
        i+=1
        print(i)
        j-=1
    print(arr_str)


print(reverse_a_string(['h','e','l','l','o']))
