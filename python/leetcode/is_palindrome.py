def isPalindrome(n):
    num_str = str(n)
    reversed_num = num_str[::-1]
    if num_str ==  reversed_num:
        return True
    return False


print(isPalindrome(121)) #True
print(isPalindrome(-121)) #False
print(isPalindrome(10)) #false
print(isPalindrome(1940340)) #false
