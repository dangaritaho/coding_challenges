def flatten_array(array):
    # create an empty array
    # loop through the array
    # check if the elem is a list, if it it keep looping
    # otherwise append into the empty array
    # append the elements into the empty array
    # return the flatten array

    flatten_arr = []
    for ele in array:
        if isinstance(ele, list):
            for ele1 in ele:
                if isinstance(ele1, list):
                    for ele2 in ele1:
                        flatten_arr.append(ele2)
                else:
                    flatten_arr.append(ele1)
        else:
            flatten_arr.append(ele)
    return flatten_arr


print(flatten_array([1, [2, 3], [4, [5, 6]], 7]))
# output [1, 2, 3, 4, 5, 6, 7]

print(flatten_array([[1, 2], [3, 4], [5, 6]]))
# output [1, 2, 3, 4, 5, 6]


def flatten_array(array):
    # create an empty array
    # loop through the array
    # check if the element is a list and recursively call the function to flatten the list
    # if it isn't a list, append the element into the empty array
    # return the flatten array

    flatten_arr = []
    for ele in array:
        if isinstance(ele, list):
            flatten_arr.extend(flatten_array(ele))
        else:
            flatten_arr.append(ele)
    return flatten_arr


print(flatten_array([1, [2, 3], [4, [5, 6]], 7]))
# output [1, 2, 3, 4, 5, 6, 7]

print(flatten_array([[1, 2], [3, 4], [5, 6]]))
# output [1, 2, 3, 4, 5, 6]
