def miniMaxSum(arr):
    #sort the array
    # print a copy of the array removing the first element and last element
    # use sum built-in function and the min and max sum
    sorted_array = sorted(arr)
    minimum_sum = sum(sorted_array[:-1])
    maximum_sum = sum(sorted_array[1:])
    print(minimum_sum, maximum_sum)


print(miniMaxSum([1,3,5,7,9]))
print(miniMaxSum([122,3,5,57,9]))
