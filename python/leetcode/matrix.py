def rotate_matrix(a): # IN CASE I NEED TO POP THE FIRST 2 INTEGERS
    print(a)
    new_matrix = a[2:]
    print(new_matrix)
    rows = len(new_matrix)
    cols = len(new_matrix[0])
    print(cols)
    print(rows)
    print(cols)
    res = []

    for i in range(cols):
        temp = []
        for j in range(rows):
            temp.append(new_matrix[j][i])
            print(temp)
        res.append(temp[::-1])
    print(res)
    return res

input1 = [
    3, 3,
    [1,2,3],
    [4,5,6],
    [7,8,9]
]
print(rotate_matrix(input1))

#output
"""
[7,4,1],
[8,5,2],
[9,6,3]

"""


# SOLUTION 2
def rot_mat(a):
    print(a)
    rows = len(a)
    cols = len(a[0])
    print(rows)
    print(cols)
    res = []

    for i in range(cols):
        temp = []
        for j in range(rows):
            temp.append(a[j][i])
            # print(temp)
        res.append(temp[::-1])
    print(res)
    return res

input = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]
print(rot_mat(input))
