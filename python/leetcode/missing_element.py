def one_element_removed(full, missing):
    missing_ele = 0
    for i in full:
        print(i)
        missing_ele = missing_ele ^ i
        print(missing_ele)
        for i in missing:
            missing_ele = missing_ele^i
    return missing_ele


print(one_element_removed([1, 2, 3, 4, 5],[1, 2, 3, 5] ))

# solution 2

def one_element_removed(full, missing):
    for i in missing:
        full.remove(i)
    return full[0]

print(one_element_removed([1, 2, 3, 4, 5],[1, 2, 3, 5] ))
