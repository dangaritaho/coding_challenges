def is_pangrams(s):
    lower_s= list(set(s.lower()))
    print(lower_s)
    alphabet = list("abcdefghijklmnopqrstuvwxyz")
    c = 0
    if len(lower_s)<26:
        return "not pangram"
    for char in lower_s:
        if char in alphabet:
            c+=1
    if c < 26: return "not pangram"
    else: return "pangram"


print(is_pangrams('Hello World'))
print(is_pangrams('Hello World, it is almost winter'))
print(is_pangrams('pack my box with five dozen liquor jugs'))
print(is_pangrams('this is not a'))
print(is_pangrams('We promptly judged antique ivory buckles for the prize'))

