# SOLUTION 1

def find_max(nums):
    if len(nums) == 0:
        return None
    return sorted(nums)[-1]


print(find_max([1, 7, 3, 5, 6])) # 7

print(find_max([])) # None

print(find_max([4,-6,3,10,11,150,120]))

# SOLUTION 2

def find_max(nums):
    if len(nums) == 0:
        return None
    return max(nums)


print(find_max([1, 7, 3, 5, 6])) # 7

print(find_max([])) # None

print(find_max([4,-6,3,10,11,150,120]))

# SOLUTION 3

def find_max(nums):
    if len(nums) == 0:
        return None
    maximum = 0
    for num in nums:
        if num > maximum:
            maximum = num
    return maximum


print(find_max([1, 7, 3, 5, 6])) # 7

print(find_max([])) # None

print(find_max([4,-6,3,10,11,150,120]))
