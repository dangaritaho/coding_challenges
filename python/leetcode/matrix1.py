def matrix(employees):
    # each row is a company
    # each column represents a dpt
    # result = # of employees / total employees in the company

    percentage_by_dpt = []

    for i in employees:
        row_sum = sum(i)
        temp_percentage = []
        for j in i:
            temp_percentage.append(j / row_sum)
        percentage_by_dpt.append(temp_percentage)
    return percentage_by_dpt

input = [
    [10, 20, 30, 30, 10],
    [15, 15, 5, 10, 5],
    [150, 50, 100, 150, 50],
    [300, 200, 300, 100, 100],
    [1, 5, 1, 1, 2]
]

print(matrix(input))
