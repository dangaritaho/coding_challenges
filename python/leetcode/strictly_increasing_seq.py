def strictly_increasing_seq(sequence):
    remove_element = False

    for i in range(1,len(sequence)):
        current_number = sequence[i]
        previous_number = sequence[i-1]
        if current_number<= previous_number: # if the cond is meet,
            if remove_element: # the condition for removal is met
                return False # False, because another removal cannot be made
            if i > 1 and current_number <= sequence[i-2]:
                current_number = previous_number 
            remove_element = True # the removal was done
    return True




print(strictly_increasing_seq([1,3, 2,1])) #false
print(strictly_increasing_seq([1,3,2])) #true
2<=3
