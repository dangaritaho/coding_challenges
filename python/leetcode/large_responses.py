def large_responses(filename):
    list2 = []  # Store bytes values

    # Read the .txt file
    with open(filename, "r") as f: #"r" read mode
        lines = f.readlines()

    # get & print the information
    print("Host name   Time Stamp  \t\t\tRequest\t\t\tResponse   Code  Bytes")
    for line in lines[1:]:  # Skip the header row
        parts = line.split('|')
        host_name = parts[1].strip()
        time_stamp = parts[3].strip()
        request = parts[5].strip()
        response_code = parts[6].strip()

        # handle error when there's no nums in the bytes colum
        try:
            bytes_value = int(parts[7].strip())
            list2.append(bytes_value)
        except ValueError:
            bytes_value = 0

        print(f"{host_name} {time_stamp} {request} {response_code} {bytes_value}")

    # results
    print("................................")
    print("Hosts, which sent more than 5000 Bytes are")
    high_bytes_hosts = [x for x in list2 if x > 5000]
    for bytes_value in high_bytes_hosts:
        print(f"Bytes: {bytes_value}")

    c = len(high_bytes_hosts)
    sum1 = sum(high_bytes_hosts)
    sum2 = 0
    for x in list2:
        if x<=500: sum2+= x

    print("No. of hosts:", c)
    print("total bytes sent > 5000: ", sum1)
    print("Total bytes sent < less than 5000: ", sum2)

    # outputs if conditions are met
    limit = 10 ** 12
    output_file = "bytes_" + filename
    if sum1 <= limit and sum1 > 5000:
        print(f"Data (for bytes > 5000, it's {sum1}) written to file")
        with open(output_file, "w") as f1:
            f1.write(f"{c}\n{sum1}")
    else:
        print(f"Bytes sent are < 5000, it's {sum2}, data not written to the file")

if __name__ == "__main__":
    filename = "host_access_log_00.txt"
    large_responses(filename)
