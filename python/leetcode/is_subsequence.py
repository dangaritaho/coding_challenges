class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        len_s = len(s)
        len_m= len(t)
        i = 0
        j = 0

        while i < len_s and j < len_m:
            if s[i] == t[j]:
                i += 1
            j += 1
        return i == len_s


solution = Solution()
print(solution.isSubsequence("abc","ahbgdc"))
print(solution.isSubsequence("axc","ahbgdc"))
print(solution.isSubsequence("bb","ahbgdc"))
