def adjacent_elements(inputArray):
    result = []

    for i in range(len(inputArray)-1):
        product = inputArray[i] * inputArray[i+1]
        result.append(product)

    max_prod = max(result)
    return max_prod

print(adjacent_elements([3, 6, -2, -5, 7, 3]))
print(adjacent_elements([9, 5, 10, 2, 24, -1, -48]))
print(adjacent_elements([1, 0, 0, 0, 5000]))
print(adjacent_elements([1, 0, 1, 0, 1000]))


#ANOTHER WAY OF SOLVING IT
def adjacent_elements(inputArray):
    max_product = float('-inf')

    for i in range(len(inputArray) - 1):
        product = inputArray[i] * inputArray[i + 1]
        max_product = max(max_product, product)
    return max_product

print(adjacent_elements([3, 6, -2, -5, 7, 3]))
print(adjacent_elements([9, 5, 10, 2, 24, -1, -48]))
print(adjacent_elements([1, 0, 1, 0, 1000]))
