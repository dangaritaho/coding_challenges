def roman_to_int(s):
    roman = {
        "I": 1,
        "IV": 4,
        "V": 5,
        "IX": 9,
        "X": 10,
        "XL": 40,
        "L": 50,
        "XC": 90,
        "C": 100,
        "CD": 400,
        "D": 500,
        "CM": 900,
        "M": 1000,
    }
    i = 0
    num = 0
    while i < len(s):
        print(i)
        print(s[i:i+2])
        if i + 1 < len(s) and s[i:i+2] in roman:
            num += roman[s[i:i+2]]
            print(num)
            i+=2
        else:
            num+=roman[s[i]]
            print(i)
            i += 1
    return num


print(roman_to_int("III"))
print(roman_to_int("LVIII"))
print(roman_to_int("MCMXCIV"))


class Solution:
    def romanToInt(self, s):

        values = {'I': 1, 'V': 5, 'X': 10, 'L' : 50, 'C' : 100, 'D' : 500, 'M' : 1000}
        result = 0
        for i in range(len(s)):
            if i + 1 < len(s) and values[s[i]] < values[s[i + 1]] : # if current item is not the last item on the string
                                                                    # and current item's value is smaller than next item's value
                result = result - values[s[i]]                      # then subtract current item's value from result
            else:
                result = result + values[s[i]]                      # otherwise add current item's value to result
        return result

Task = Solution()
# print(Task.romanToInt("III"))
# print(Task.romanToInt("LVIII"))
print(Task.romanToInt("MCMXCIV"))
