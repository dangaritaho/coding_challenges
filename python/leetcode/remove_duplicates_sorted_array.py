def remove_duplicates_sorted_array(nums):

    curr_idx = 2

    for i in range(2, len(nums)):
        current_num = nums[i]
        if current_num != nums[curr_idx - 2]:
            nums[curr_idx] = current_num
            curr_idx +=1
    return curr_idx


print(remove_duplicates_sorted_array([1,1,1,2,2,3]))
