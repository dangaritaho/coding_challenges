def longest_common_prefix(strs):
    # if the string is empty
        # return an empty string
    # initialize a prefix variable with the first index
    # loop through the rest of the string
        # while the string at the current index  doesn't start with the current value of the prefix
        #  Remove the last character from the prefix
    # return prefix

    if len(strs) == 0: return ""
    if len(strs) == 1: return prefix
    prefix = strs[0]
    for i in range(1, len(strs)):
        while not strs[i].startswith(prefix):
            prefix = prefix[:-1]
            # print(prefix)
    return prefix




print(longest_common_prefix(['flower', 'flow', 'flight']))
print(longest_common_prefix(["dog","racecar","car"]))
