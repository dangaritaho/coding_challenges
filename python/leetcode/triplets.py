def triplets(num, arr):
    '''
    -triplets
    -the length of the triplets cannot be more than the num
    '''
    result = 0
    for i in range(len(arr) - 2): #first element
        for j in range(i + 1, len(arr) - 1): # second element
            for k in range(j + 1, len(arr)): #third element
                if arr[i] + arr[j] + arr[k] <= num:
                    result += 1
    return result

print(triplets(8, [1, 2, 3, 4, 5]))
#5,2,1
#4,3,1
