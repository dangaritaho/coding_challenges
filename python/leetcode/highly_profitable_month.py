def count_highly_profitable_months(stock_prices, n, k):
    count = 0

    for i in range(n - k + 1):
        strictly_increasing = 1

        for j in range(i + 1, i + k):
            if stock_prices[j] <= stock_prices[j - 1]:
                strictly_increasing = 0
                break

        if strictly_increasing:
            count += 1

    return count


stock_prices = [5,3,5,7,8]
n = len(stock_prices)
k = 3
result = count_highly_profitable_months(stock_prices, n, k)
print(result)





# def break_palindrome(inp):
#     length = len(inp)

#     # Case 1: If the length is even, modify the first non-'a' character to 'a'.
#     if length % 2 == 0:
#         for i in range(length):
#             if inp[i] != 'a':
#                 inp = inp[:i] + 'a' + inp[i + 1:]
#                 return inp  # Return the modified string as the result.

#     else:  # Case 2: If the length is odd, make sure not to modify the middle character to 'a'.
#         for i in range(length):
#             if inp[i] != 'a' and i != length // 2:
#                 inp = inp[:i] + 'a' + inp[i + 1:]
#                 return inp  # Return the modified string as the result.

#     # If no valid modification is found, return "IMPOSSIBLE".
#     return "IMPOSSIBLE"

# input_str = "abccba"
# result = break_palindrome(input_str)
# print(result)


# import heapq

# def minimum_total_cost(nums):
#     # Create a min heap using heapq
#     heapq.heapify(nums)

#     total_cost = 0

#     # While there are more than one element in the heap
#     while len(nums) > 1:
#         # Extract the two smallest elements
#         num1 = heapq.heappop(nums)
#         num2 = heapq.heappop(nums)

#         # Calculate the cost of the move and add it to total_cost
#         cost = num1 + num2
#         total_cost += cost

#         # Calculate the new element and insert it into the heap
#         new_num = num1 + num2
#         heapq.heappush(nums, new_num)

#     return total_cost

# # Example usage:
# nums = [5, 3, 8, 2]
# result = minimum_total_cost(nums)
# print("Minimum total cost:", result)



# def is_even(n):
#     return n % 2 == 0

# def longest_even_word(sentence):
#     longest_even = ""  # Initialize variable to store the longest even word
#     max_even_length = 0  # Initialize variable to store the maximum even length found so far

#     # Use split to separate words from the sentence
#     words = sentence.split()

#     for word in words:
#         word_length = len(word)

#         # Check if the current word has an even length and is longer than the previously found longest even word
#         if is_even(word_length) and word_length >= max_even_length:
#             longest_even = word  # Update longest_even with the current word
#             max_even_length = word_length  # Update max_even_length with the length of the current word

#     return longest_even  # Return the first word with the longest even length found

# # Example usage:
# sentence1 = "Time to write great code"
# result1 = longest_even_word(sentence1)
# print("Output 1:", result1)

# sentence2 = "The sun sets at midnight"
# result2 = longest_even_word(sentence2)
# print("Output 2:", result2)

# sentence3 = "This is an example"
# result3 = longest_even_word(sentence3)
# print("Output 3:", result3)



# def find_missing_words(s, t):
#     result = []

#     # Split the strings into lists of words
#     words_s = s.split()
#     words_t = t.split()

#     # Iterate through words in s
#     for word in words_s:
#         if word not in words_t:
#             result.append(word)
#         else:
#             words_t.remove(word)

#     return result

# # Example usage:
# s = "I like cheese"
# t = "I cheese"
# result = find_missing_words(s, t)

# # Output the result
# print("Output:", result)




# def count_ones_in_binary(num):
#     # Count the number of '1's in the binary representation of the number
#     return bin(num).count('1')

# def ascending_binary_sorting(nums):
#     # Sort the numbers based on the count of '1's and then by their decimal values
#     nums.sort(key=lambda x: (count_ones_in_binary(x), x))
#     return nums

# # Example usage:
# nums1 = [5, 3, 7, 10]
# result1 = ascending_binary_sorting(nums1)
# print("Output 1:", result1)

# nums2 = [1, 2, 3, 4, 5]
# result2 = ascending_binary_sorting(nums2)
# print("Output 2:", result2)



def is_possible(a, b, c, d):
    if a == c and b == d:
        return True
    elif a > c or b > d:
        return False
    return is_possible(a + b, b, c, d) or is_possible(a, a + b, c, d)
