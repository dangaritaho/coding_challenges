def interesting_polygon(n):
    """
        Find the a_inter_polygon = ? for a given n


    """
    if n == 1: return n
    area = (n*n) + (n-1)* (n-1)
    print(area, "this is area")
    return area

print(interesting_polygon(1))
print(interesting_polygon(2))
print(interesting_polygon(3))
print(interesting_polygon(4))
print(interesting_polygon(5))
