def merge_list(input1, input2):
    result = sorted(input1 + input2)
    return result

print(merge_list([1,2,5], [2,4,6])) #[1,2,2,4,5,6]
print(merge_list([1,5,2], [])) # [1,2,5]
print(merge_list([], [])) # []
print(merge_list([], [1])) # []


# sol 2. using bubble sort after merging the two arrays
def merge_list(input1, input2):
    merged_list = (input1 + input2)

    len_list = len(merged_list)
    for i in range(len_list - 1, 0, -1):
        for j in range(0, i-1):
            if merged_list[j]>merged_list[j+1]:
                merged_list[j], merged_list[j+1] = merged_list[j+1], merged_list[j]
            else:
                pass

    return merged_list

print(merge_list([1,2,5], [2,4,6])) #[1,2,2,4,5,6]
print(merge_list([1,5,2], [])) # [1,2,5]
print(merge_list([], [])) # []
print(merge_list([], [1])) # []
