def remove_element(nums, val):
    idx = 0 #current position non-val num
    for i in range(len(nums)):
        if nums[i] != val:
            nums[idx] = nums[i]
            idx +=1

    return idx #new length of the modified array



print(remove_element([3, 2, 2, 3], 3))
print(remove_element([0,1,2,2,3,0,4,2], 2))
