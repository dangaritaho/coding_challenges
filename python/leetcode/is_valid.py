class Solution:
    def is_valid(self, s: str) -> bool:
        stack = [] #only used to stack elements
        pairs = {
            "(" : ")",
            "[" : "]",
            "{" : "}"
        }
        print(pairs)
        for bracket in s:
            print(bracket)
            if bracket in pairs:
                print(bracket)
                stack.append(bracket)
            elif len(stack) == 0 or bracket != pairs[stack.pop()]:
                return False
        return len(stack) == 0

solution = Solution()
print(solution.is_valid("()[]{}"))
print(solution.is_valid("()"))
print(solution.is_valid("(]"))
print(solution.is_valid("{[]}"))
