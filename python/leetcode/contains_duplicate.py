
def containsDuplicate(nums):
    # sort the array
    # for loop to check every element in the array
        # if the elems compared are duplicates,
            # return True
    # return false

    sorted_array = sorted(nums)

    for i in range(1, len(sorted_array)):
        if sorted_array[i-1] == sorted_array[i]: return True
    return False

print(containsDuplicate([2,14,18,22,22]))
