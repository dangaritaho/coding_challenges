class Solution:
    def letterCombinations(self, digits: str):
        if len(digits) == 0:
            return []

        phone = {
            "2":"abc",
            "3":"def",
            "4":"ghi",
            "5":"jkl",
            "6":"mno",
            "7":"pqrs",
            "8":"tuv",
            "9":"wxyz"
        }
        result = [""]

        for i in digits:
            sol = []
            for comb2 in phone[i]:
                for comb1 in result:
                    sol.append(comb1+comb2)
            result = sol
        return result

solution = Solution()
print(solution.letterCombinations("23"))
print(solution.letterCombinations(""))
print(solution.letterCombinations("2"))
print(solution.letterCombinations("45"))
print(solution.letterCombinations("24523"))
