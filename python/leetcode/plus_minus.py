def plusMinus(arr):
    # Write your code here
    n = len(arr)
    positive_counter = 0
    negative_counter = 0
    zero_counter = 0
    for ele in arr:
        if ele > 0:
            positive_counter += 1
        elif ele < 0:
            negative_counter +=1
        else:
            zero_counter += 1
    print(f"{positive_counter/n:.6f}")
    print(f"{negative_counter/n:.6f}")
    print(f"{zero_counter/n:.6f}")


print(plusMinus([-4, 3, -9, 0, 4, 1]))
