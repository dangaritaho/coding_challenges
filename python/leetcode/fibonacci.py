def fibonacci_recursive(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)

print(fibonacci_recursive(10))

# method 2
def fibonacci_iterative(n):
    a = 0
    b = 1
    for i in range(n):
        a, b = b, a + b
    return a

print(fibonacci_iterative(10))

#method 3
def fibonacci_memoization(n, memo={}):
    if n <= 1:
        return n
    elif n not in memo:
        memo[n] = fibonacci_memoization(n-1, memo) + fibonacci_memoization(n-2, memo)
    return memo[n]
print(fibonacci_memoization(10))
