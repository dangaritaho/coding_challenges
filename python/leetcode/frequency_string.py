from collections import Counter

def top_k_frequent(words, k):
    # count the frequency
    frequency = Counter(words)

    #create a sorting funct to consider frequency and word
    def sorting(word):
        return (-frequency[word], word)

    #sort the words using the sorting funct
    sorted_words = sorted(frequency.keys(), key=sorting) #sort the unique words

    #return the top k words from the sorted list
    return sorted_words[:k]


print(top_k_frequent(["i","love","leetcode","i","love","coding"], 2))
#Output: ["i","love"]

print(top_k_frequent(["the","day","is","sunny","the","the","the","sunny","is","is"], 4))
# Output: ["the","is","sunny","day"]
