def count(input1, input2):
    count =0
    for num in range(1,input1 +1):
        num_str = str(num)

        d_sum = 0
        for digit in num_str:
            d_sum = int(digit)

        if d_sum == input2:
            count += 1

    if count == 0:
        return -1
    else:
        return count


print(count(20, 5))
