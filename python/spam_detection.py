import re

def email_spam_detection(subjects, spam_words):
    def count_spam_words(subject, spam_words):
        subject_lower = subject.lower()
        spam_count = 0

        for word in spam_words:
            spam_count += len(re.findall(r'\b' + re.escape(word.lower()) + r'\b', subject_lower))

        return spam_count

    results = []

    for subject in subjects:
        spam_count = count_spam_words(subject, spam_words)
        if spam_count >= 2:
            results.append('spam')
        else:
            results.append('not_spam')

    return results


print(email_spam_detection(['I paid him paid', 'Summertime Sadness'], ['i', 'Sadness', 'paid']))
print(email_spam_detection(['let it go', 'the right thing to do'], [ 'to', 'do', 'right', 'go', 'let']))
print(email_spam_detection(['free prize worth millions', 'ten tips for a carefree lifestyle'], ['free', 'money', 'win', 'millions']))
