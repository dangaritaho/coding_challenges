class Solution:
    # Function to find largest and second largest element in the array
    def largestAndSecondLargest(self, sizeOfArray, arr):
        # Your code here'''
        # Function should return a list containing two elements'''
        # li = [maximum, second_maximum]
        sorted_arr = sorted(arr, reverse=True)
        maximum = sorted_arr[0]
        second_maximum = 0
        for elem in sorted_arr:
            if elem != maximum:
                second_maximum = elem
                break
        else: second_maximum = -1
        return [maximum, second_maximum]






solution = Solution()
print(solution.largestAndSecondLargest(2, [2,1,2]))
