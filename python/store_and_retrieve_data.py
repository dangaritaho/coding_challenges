"""
    Design a data structure to efficiently store and retrieve historical trade data
    for multiple stocks, including the price and volume at each timestamp.
"""

trade_data = {}

trade_record = {
    'price': 100.0,
    'volume': 500.0,
    'timestamp': '2023-06-20 14:30:00'
}

def add_trade_record(stock_symbol, trade_record):
    if stock_symbol in trade_data: trade_data[stock_symbol].append(trade_record)
    else: trade_data[stock_symbol] = [trade_record]


def get_trade_data(stock_symbol):
    if stock_symbol in trade_data: return trade_data[stock_symbol]
    else: return []

trade_record1 = {
    'price': 100.0,
    'volume': 500,
    'timestamp': '2023-06-20 14:30:00'
}

trade_record2 = {
    'price': 105.0,
    'volume': 700,
    'timestamp': '2023-06-20 14:35:00'
}

add_trade_record('AAPL', trade_record1)
add_trade_record('AAPL', trade_record2)

stock_data = get_trade_data('AAPL')
print(stock_data)
