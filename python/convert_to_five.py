# Function should return an integer value
def convertFive(n):

    num = str(n)
    new_num = num.replace("0", "5")
    return int(new_num)


print(convertFive(1004))


def convertFive(n):
    # convert n to a string
    # iterate thought the string
        # if there is any 0
            # convert it to 5
        # otherwise
            # return n
    # return result
    num = str(n)
    result = ""
    for dig in num:
        if dig == "0":
            result += "5"
        else:
            result += dig
    return int(result)

print(convertFive(1004))
