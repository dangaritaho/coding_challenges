def bubble_sort(array):
    for i in range(len(array)):
        for j in range(len(array) - i - 1):
            if array[j] > array[j + 1]:
                array[j], array[j + 1] = array[j + 1], array[j]

my_list = [40, 3, 7, 12, -1, -5, 50, 1, 4, 40, 3, 50]
bubble_sort(my_list)
print(my_list)
