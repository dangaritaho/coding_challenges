import math

def factorial_given_number(number):
    # solution 1
    # return math.factorial(number)

    # solution 2
    result = 1
    for i in range(1, number + 1):
        result *= i
    return result

print(factorial_given_number(5))
