def is_anagram(string):
  alphabet ='abcdefghijklmnopqrstuvwxyz'
  missing_char =''

  lower_str =(string.lower())
  for char in alphabet:
    if char not in lower_str: missing_char += char
  return missing_char

print(is_anagram('Hello World'))
print(is_anagram('pack my box with five dozen liquor jugs'))
print(is_anagram('this is not a'))
