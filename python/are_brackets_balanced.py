from stack import Stack
def is_match(p1, p2):
    if (p1 == "(" and p2 == ")" or
        p1 == "{" and p2 == "}" or
        p1 == "[" and p2 =="]"): return True
    return False

def are_brackets_balanced(string):
    s = Stack()

    for ele in string:
        if ele == '(' or ele == "{" or ele == "[":
            s.push(ele)
            print(s.get_stack())
        elif ele in ")]}":
            if s.is_empty(): return False
            print(s.is_empty())
            top_stack = s.pop()
            print(s.get_stack())
            if not is_match(top_stack, ele): return False

    if s.is_empty(): return True
    return False

#balanced
print(are_brackets_balanced("( ( { [ ] } ) )"))
print(are_brackets_balanced("{ } { }"))
print(are_brackets_balanced("{ }"))

#unbalanced
print(are_brackets_balanced("( ( )"))
print(are_brackets_balanced("{ { { ) } ]"))
print(are_brackets_balanced("[ ] [ ] ] ]"))
