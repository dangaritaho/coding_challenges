def close_sum_to_zero(array):
    # create a new var with the target value of 0
    # create a result var
    # iterate over the length of the array
        # calculate the sum of the current pair
        # calculate the difference from the target value
        #check if the current difference is smaller than the closest difference
            # if it is, update the current closest_sum var
            # update the current result with the current pair
    target = 0
    result = []
    closest_sum = float('inf')

    for i in range(len(array) - 1):
        for j in range(i + 1, len(array)):
            current_sum = array[i] + array[j]
            diff = abs(current_sum - target)

            if diff < abs(closest_sum - target):
                closest_sum = current_sum
                result = [array[i], array[j]]
    return result

print(close_sum_to_zero([0.5, 1, 11, 2, 4, 6, 2, 1, 5, 5]))
