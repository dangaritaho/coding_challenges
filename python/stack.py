class Stack():
    # push
    # pop
    # peek
    def __init__(self):
        self.items = []

    def push(self, items):
        self.items.append(items)

    def pop(self):
        return self.items.pop()

    def get_stack(self):
        return self.items

    def is_empty(self):
        if self.items == []: return True
        return False

    def peek(self):
        if not self.is_empty(): return self.items[-1]
        return None



myStack = Stack()
print(myStack.is_empty())
print(myStack.get_stack())
myStack.push(1)
myStack.push(2)
myStack.push(3)
myStack.push(4)
print(myStack.get_stack())
myStack.push("Diana")
myStack.push("is")
myStack.push("learning")
myStack.pop()
print(myStack.get_stack())
myStack.push("about")
myStack.push("stacks")
myStack.pop()
print(myStack.get_stack())
